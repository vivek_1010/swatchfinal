-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 02, 2016 at 08:03 AM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ebiz7`
--

-- --------------------------------------------------------

--
-- Table structure for table `intrm$cntry`
--

CREATE TABLE `intrm$cntry` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(5) DEFAULT NULL,
  `CNTRY_ID` int(20) DEFAULT NULL,
  `POS_CNTRY_ID` int(5) DEFAULT NULL,
  `CNTRY_DESC` varchar(50) NOT NULL,
  `CURR_ID` varchar(5) NOT NULL,
  `CREATE_FLG` varchar(1) DEFAULT NULL,
  `UPD_FLG` varchar(1) DEFAULT NULL,
  `DEL_FLG` varchar(1) DEFAULT NULL,
  `USR_ID_CREATE` int(10) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(10) DEFAULT NULL,
  `USR_ID_MOD_DT` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$curr`
--

CREATE TABLE `intrm$curr` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(5) DEFAULT NULL,
  `CURR_ID` int(20) DEFAULT NULL,
  `POS_CURR_ID` int(20) DEFAULT NULL,
  `CURR_NM` varchar(50) NOT NULL,
  `CURR_NOTATION` varchar(40) NOT NULL,
  `CREATE_FLG` varchar(1) DEFAULT NULL,
  `UPD_FLG` varchar(1) DEFAULT NULL,
  `DEL_FLG` varchar(1) DEFAULT NULL,
  `USR_ID_CREATE` int(10) NOT NULL,
  `USR_ID_CREATE_DT` date NOT NULL,
  `USR_ID_MOD` int(10) NOT NULL,
  `USR_ID_MOD_DT` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$curr$conv`
--

CREATE TABLE `intrm$curr$conv` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(5) DEFAULT NULL,
  `CURR_ID` varchar(20) DEFAULT NULL,
  `POS_CURR_ID` int(20) DEFAULT NULL,
  `CC_EFF_DATE` date DEFAULT NULL,
  `CC_EFF_TO_DATE` date DEFAULT NULL,
  `POS_CURR_ID_TXN` int(20) DEFAULT NULL,
  `CURR_ID_TXN` int(5) DEFAULT NULL,
  `CC_BUY` int(26) DEFAULT NULL,
  `CC_SELL` int(26) DEFAULT NULL,
  `CREATE_FLG` varchar(1) DEFAULT NULL,
  `UPD_FLG` varchar(1) DEFAULT NULL,
  `DEL_FLG` varchar(1) DEFAULT NULL,
  `USR_ID_CREATE` int(10) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(10) NOT NULL,
  `USR_ID_MOD_DT` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$eo`
--

CREATE TABLE `intrm$eo` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(2) DEFAULT NULL,
  `HO_ORG_ID` varchar(2) DEFAULT NULL,
  `EO_TYPE` varchar(1) DEFAULT NULL,
  `EO_ID` varchar(20) DEFAULT NULL,
  `LOYALITY_EO_ID` varchar(20) DEFAULT NULL,
  `POS_EO_ID` int(20) DEFAULT NULL,
  `LOYALITY_POS_EO_ID` varchar(20) DEFAULT NULL,
  `EO_CATG_ID` varchar(40) DEFAULT NULL,
  `EO_ALIAS` varchar(20) DEFAULT NULL,
  `EO_LEG_CODE` varchar(20) DEFAULT NULL,
  `EO_NM` varchar(250) NOT NULL,
  `EO_CNTRY_ID` varchar(50) DEFAULT NULL,
  `EO_ACC_ID` varchar(20) DEFAULT NULL,
  `AVG_CR_DAYS` int(5) DEFAULT NULL,
  `CR_LIMIT` int(15) DEFAULT NULL,
  `AVG_DLV_DAYS` int(5) DEFAULT NULL,
  `EO_CURR_ID` varchar(20) DEFAULT NULL,
  `TDS_DIVISION` varchar(50) DEFAULT NULL,
  `TDS_COLLECTORATE` varchar(50) DEFAULT NULL,
  `TDS_RANGE` varchar(50) DEFAULT NULL,
  `PAN_GIR_NO` varchar(50) DEFAULT NULL,
  `REGN_CERTI_NO` varchar(50) DEFAULT NULL,
  `ECC_NO` varchar(50) DEFAULT NULL,
  `SERVICE_TAX_NO` varchar(50) DEFAULT NULL,
  `TIN_NO` varchar(50) DEFAULT NULL,
  `VAT_NO` varchar(50) DEFAULT NULL,
  `ON_HOLD` varchar(1) DEFAULT NULL,
  `BLACK_LISTED` varchar(1) DEFAULT NULL,
  `BLACKLIST_RESN` varchar(200) DEFAULT NULL,
  `BLACKLIST_FRM_DT` date DEFAULT NULL,
  `BLACKLIST_TO_DT` date DEFAULT NULL,
  `ONHOLD_RESN` varchar(200) DEFAULT NULL,
  `ONHOLD_FRM_DT` date DEFAULT NULL,
  `ONHOLD_TO_DT` date DEFAULT NULL,
  `EO_SMAN_ID` varchar(20) DEFAULT NULL,
  `ALLOW_RET_GOODS` varchar(1) DEFAULT NULL,
  `EO_CHECK_ORD_ADV` varchar(2) DEFAULT NULL,
  `EO_ORD_ADV_TYPE` varchar(2) DEFAULT NULL,
  `EO_ORD_ADV_VAL` int(26) DEFAULT NULL,
  `CST_NO` varchar(50) DEFAULT NULL,
  `CST_DT` date DEFAULT NULL,
  `CIN_NO` varchar(50) DEFAULT NULL,
  `USR_ID_CREATE` int(4) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(4) NOT NULL,
  `USR_ID_MOD_DT` date NOT NULL,
  `EO_PHONE` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$eo$add`
--

CREATE TABLE `intrm$eo$add` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(2) DEFAULT NULL,
  `HO_ORG_ID` varchar(2) DEFAULT NULL,
  `EO_ID` varchar(20) DEFAULT NULL,
  `EO_TYPE` varchar(1) NOT NULL,
  `POS_EO_ID` int(20) DEFAULT NULL,
  `EO_ADD_TYPE` varchar(1) DEFAULT NULL,
  `ADDRESS_ID` varchar(40) DEFAULT NULL,
  `POS_ADDRESS_ID` int(20) DEFAULT NULL,
  `ADDRESS` varchar(250) NOT NULL,
  `DEF_ADD_FLG` varchar(1) NOT NULL,
  `USR_ID_CREATE` int(4) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(4) DEFAULT NULL,
  `USR_ID_MOD_DT` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$eo$add$org`
--

CREATE TABLE `intrm$eo$add$org` (
  `CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(2) NOT NULL,
  `HO_ORG_ID` varchar(2) NOT NULL,
  `ORG_ID` varchar(2) NOT NULL,
  `EO_ID` varchar(20) NOT NULL,
  `EO_TYPE` varchar(1) NOT NULL,
  `POS_EO_ID` int(20) DEFAULT NULL,
  `ADDRESS_ID` varchar(40) DEFAULT NULL,
  `POS_ADDRESS_ID` int(20) DEFAULT NULL,
  `CREATE_FLG` varchar(1) DEFAULT NULL,
  `UPD_FLG` varchar(1) DEFAULT NULL,
  `DEL_FLG` varchar(1) DEFAULT NULL,
  `USR_ID_CREATE` int(4) NOT NULL,
  `USR_ID_CREATE_DT` date NOT NULL,
  `USR_ID_MOD` int(4) NOT NULL,
  `USR_ID_MOD_DT` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$eo$catg`
--

CREATE TABLE `intrm$eo$catg` (
  `CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(5) NOT NULL,
  `HO_ORG_ID` varchar(2) NOT NULL,
  `EO_TYPE` varchar(5) NOT NULL,
  `EO_CATG_ID` varchar(50) DEFAULT NULL,
  `POS_EO_CATG_ID` int(5) DEFAULT NULL,
  `CATG_NM` varchar(50) DEFAULT NULL,
  `CREATE_FLG` varchar(1) DEFAULT NULL,
  `UPD_FLG` varchar(1) DEFAULT NULL,
  `DEL_FLG` varchar(1) DEFAULT NULL,
  `USR_ID_CREATE` int(10) DEFAULT NULL,
  `USR_ID_CREATE_DT` date NOT NULL,
  `USR_ID_MOD` int(10) NOT NULL,
  `USR_ID_MOD_DT` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$eo$org`
--

CREATE TABLE `intrm$eo$org` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(11) DEFAULT NULL,
  `HO_ORG_ID` varchar(2) DEFAULT NULL,
  `ORG_ID` varchar(2) DEFAULT NULL,
  `EO_ID` varchar(20) DEFAULT NULL,
  `EO_TYPE` varchar(1) NOT NULL,
  `LOYALITY_EO_ID` varchar(20) DEFAULT NULL,
  `POS_EO_ID` int(20) DEFAULT NULL,
  `LOYALITY_POS_EO_ID` varchar(20) DEFAULT NULL,
  `CREATE_FLG` varchar(1) DEFAULT NULL,
  `UPD_FLG` varchar(1) DEFAULT NULL,
  `DEL_FLG` varchar(1) DEFAULT NULL,
  `USR_ID_CREATE` int(4) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(4) NOT NULL,
  `USR_ID_MOD_DT` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$itm$grp`
--

CREATE TABLE `intrm$itm$grp` (
  `CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(5) NOT NULL,
  `HO_ORG_ID` varchar(2) NOT NULL,
  `POS_GRP_ID` int(20) DEFAULT NULL,
  `GRP_ID` varchar(20) NOT NULL,
  `GRP_NM` varchar(50) NOT NULL,
  `GRP_ID_PARENT` varchar(20) DEFAULT NULL,
  `USR_ID_CREATE` int(5) NOT NULL,
  `USR_ID_CREATE_DT` date NOT NULL,
  `USR_ID_MOD` int(5) NOT NULL,
  `USR_ID_MOD_DT` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$itm$grp$org`
--

CREATE TABLE `intrm$itm$grp$org` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(5) DEFAULT NULL,
  `HO_ORG_ID` varchar(2) DEFAULT NULL,
  `ORG_ID` varchar(2) DEFAULT NULL,
  `POS_GRP_ID` int(20) DEFAULT NULL,
  `GRP_ID` varchar(20) DEFAULT NULL,
  `CREATE_FLG` varchar(1) DEFAULT NULL,
  `UPD_FLG` varchar(1) DEFAULT NULL,
  `DEL_FLG` varchar(1) DEFAULT NULL,
  `USR_ID_CREATE` int(5) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(5) NOT NULL,
  `USR_ID_MOD_DT` date NOT NULL,
  `TAX_VAL` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$itm$schm`
--

CREATE TABLE `intrm$itm$schm` (
  `CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(11) NOT NULL,
  `HO_ORG_ID` varchar(2) NOT NULL,
  `POS_ITM_ID` int(11) NOT NULL,
  `ITM_ID` varchar(20) NOT NULL,
  `DOC_ID` varchar(50) NOT NULL,
  `POS_SCHM_ID` int(11) NOT NULL,
  `SCHM_ID` varchar(20) NOT NULL,
  `SCHM_NM` varchar(50) NOT NULL,
  `SCHM_DESC` varchar(200) NOT NULL,
  `SCHM_TYP` varchar(1) NOT NULL,
  `SCHM_FLG` varchar(20) NOT NULL,
  `GRP_ITM_FLG` varchar(1) NOT NULL,
  `GRP_ID` varchar(50) NOT NULL,
  `POS_GRP_ID` int(11) NOT NULL,
  `ITM_UOM` varchar(20) NOT NULL,
  `POS_ITM_UOM_ID` int(11) NOT NULL,
  `MIN_QTY` int(11) NOT NULL,
  `MAX_QTY` int(11) NOT NULL,
  `MIN_AMT` int(11) NOT NULL,
  `MAX_AMT` int(11) NOT NULL,
  `VALID_FROM` date NOT NULL,
  `VALID_TO` date NOT NULL,
  `SCHM_CALC_TYP` varchar(1) NOT NULL,
  `USR_ID_CREATE` int(11) NOT NULL,
  `USR_ID_CREATE_DT` date NOT NULL,
  `USR_ID_MOD` int(11) NOT NULL,
  `USR_ID_MOD_DT` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$itm$schm$dtl`
--

CREATE TABLE `intrm$itm$schm$dtl` (
  `CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(11) NOT NULL,
  `HO_ORG_ID` varchar(2) NOT NULL,
  `POS_ITM_ID` int(11) NOT NULL,
  `ITM_ID` varchar(50) NOT NULL,
  `SR_NO` varchar(20) NOT NULL,
  `DOC_ID` varchar(40) NOT NULL,
  `POS_SCHM_ID` int(11) NOT NULL,
  `FREE_GRP_ID` varchar(50) NOT NULL,
  `FREE_POS_GRP_ID` int(11) NOT NULL,
  `FREE_ITM_ID` varchar(20) NOT NULL,
  `FREE_POS_ITM_ID` int(11) NOT NULL,
  `FREE_ITM_UOM` varchar(20) NOT NULL,
  `FREE_POS_ITM_UOM` int(11) NOT NULL,
  `FREE_ITM_UOM_BS` varchar(20) NOT NULL,
  `FREE_POS_ITM_UOM_BS` int(11) NOT NULL,
  `FREE_ITM_QTY` int(11) NOT NULL,
  `FREE_ITM_PRICE` int(11) NOT NULL,
  `DISC_TYPE` varchar(1) NOT NULL,
  `DISC_VAL` int(11) NOT NULL,
  `FOC_FLG` varchar(1) NOT NULL,
  `CONV_FCTR` int(11) NOT NULL,
  `MIN_AMT` int(11) NOT NULL,
  `MAX_AMT` int(11) NOT NULL,
  `UNLMT_AMT` varchar(1) NOT NULL,
  `DFLT_ITM_FLG` varchar(1) NOT NULL,
  `USR_ID_CREATE` int(11) NOT NULL,
  `USR_ID_CREATE_DT` date NOT NULL,
  `USR_ID_MOD` int(11) NOT NULL,
  `USR_ID_MOD_DT` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$itm$stock$dtl`
--

CREATE TABLE `intrm$itm$stock$dtl` (
  `CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(11) NOT NULL,
  `HO_ORG_ID` varchar(2) NOT NULL,
  `ORG_ID` varchar(2) NOT NULL,
  `POS_ITM_ID` int(20) DEFAULT NULL,
  `ITM_ID` varchar(50) NOT NULL,
  `SR_NO` varchar(1) DEFAULT NULL,
  `BAR_CODE` varchar(50) DEFAULT NULL,
  `LOT_NO` varchar(50) NOT NULL,
  `UOM_BASIC` varchar(20) NOT NULL,
  `UOM_SP` varchar(20) NOT NULL,
  `conv_FCTF` varchar(20) DEFAULT NULL,
  `PRICE_SLS` int(11) NOT NULL,
  `AVAIL_QTY` int(11) NOT NULL,
  `USR_ID_CREATE` int(11) NOT NULL,
  `USR_ID_CREATE_DT` date NOT NULL,
  `USR_ID_MOD` int(11) NOT NULL,
  `USR_ID_MOD_DT` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$org`
--

CREATE TABLE `intrm$org` (
  `ORG_CLD_ID` varchar(4) DEFAULT '0000',
  `ORG_ID` varchar(2) DEFAULT NULL,
  `POS_ORG_ID` int(5) DEFAULT NULL,
  `ORG_TYPE` varchar(2) DEFAULT NULL,
  `ORG_DESC` varchar(250) DEFAULT NULL,
  `ORG_ALIAS` varchar(10) NOT NULL,
  `ORG_ID_PARENT` varchar(2) DEFAULT NULL,
  `ORG_ID_PARENT_L0` varchar(2) DEFAULT NULL,
  `ORG_COUNTRY_ID` varchar(5) NOT NULL,
  `ORG_CREATE_REF_SLOC_ID` int(2) NOT NULL,
  `ORG_CURR_ID_BS` int(5) NOT NULL,
  `ORG_TRF_ACC` int(11) NOT NULL,
  `ORG_VAT_NO` varchar(20) NOT NULL,
  `USR_ID_CREATE` int(5) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(5) NOT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$prod`
--

CREATE TABLE `intrm$prod` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(5) DEFAULT NULL,
  `HO_ORG_ID` varchar(2) DEFAULT NULL,
  `POS_ITM_ID` int(20) DEFAULT NULL,
  `ITM_ID` varchar(50) DEFAULT NULL,
  `UNIVERSAL_ITM_ID` varchar(250) NOT NULL,
  `ITM_DESC` varchar(100) NOT NULL,
  `ITM_LONG_DESC` varchar(500) NOT NULL,
  `ITM_LEGACY_CODE` varchar(50) NOT NULL,
  `GRP_ID` varchar(20) DEFAULT NULL,
  `UOM_BASIC` varchar(20) NOT NULL,
  `UOM_SLS` varchar(20) NOT NULL,
  `PRICE_BASIC` int(26) NOT NULL,
  `PRICE_SLS` int(26) NOT NULL,
  `SERIALIZED_FLG` varchar(1) NOT NULL,
  `USR_ID_CREATE` int(5) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(5) NOT NULL,
  `USR_ID_MOD_DT` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$prod$org`
--

CREATE TABLE `intrm$prod$org` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(2) DEFAULT NULL,
  `HO_ORG_ID` varchar(2) DEFAULT NULL,
  `ORG_ID` varchar(2) DEFAULT NULL,
  `POS_ITM_ID` int(20) DEFAULT NULL,
  `ITM_ID` varchar(50) DEFAULT NULL,
  `UNIVERSAL_ITM_ID` varchar(50) NOT NULL,
  `CREATE_FLG` varchar(1) DEFAULT NULL,
  `UPD_FLG` varchar(1) DEFAULT NULL,
  `DEL_FLG` varchar(1) DEFAULT NULL,
  `USR_ID_CREATE` int(5) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(5) NOT NULL,
  `USR_ID_MOD_DT` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$sls$inv`
--

CREATE TABLE `intrm$sls$inv` (
  `CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(11) NOT NULL,
  `HO_ORG_ID` varchar(2) NOT NULL,
  `ORG_ID` varchar(2) NOT NULL,
  `POS_DOC_ID` int(11) NOT NULL,
  `DOC_ID` varchar(40) NOT NULL,
  `DOC_DT` date NOT NULL,
  `EO_ID` int(11) NOT NULL,
  `POS_EO_ID` int(11) NOT NULL,
  `SLS_REP_ID` int(11) NOT NULL,
  `POS_SLS_REP_ID` int(11) NOT NULL,
  `POS_INV_TOTAL` decimal(25,4) NOT NULL,
  `TAX_BASIS` varchar(10) NOT NULL,
  `TAX_ID` int(11) NOT NULL,
  `TAX_VAL` int(11) NOT NULL,
  `DISC_BASIS` varchar(10) NOT NULL,
  `DISC_TYPE` varchar(10) NOT NULL,
  `DISC_VAL` int(11) NOT NULL,
  `DISC_TOTAL_VAL` int(11) NOT NULL,
  `SYNC_FLG` varchar(20) NOT NULL,
  `USR_ID_CREATE` int(11) NOT NULL,
  `USR_ID_CREATE_DT` date NOT NULL,
  `USR_ID_MOD` int(11) NOT NULL,
  `USR_ID_MOD_DT` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$sls$inv$itm`
--

CREATE TABLE `intrm$sls$inv$itm` (
  `CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(11) NOT NULL,
  `HO_ORG_ID` varchar(2) NOT NULL,
  `ORG_ID` varchar(2) NOT NULL,
  `POS_DOC_ID` int(11) NOT NULL,
  `DOC_ID` varchar(20) NOT NULL,
  `ITM_ID` varchar(20) NOT NULL,
  `POS_ITM_ID` int(11) NOT NULL,
  `SR_NO` varchar(20) NOT NULL,
  `LOT_NO` varchar(20) NOT NULL,
  `ITM_QTY` int(11) NOT NULL,
  `ITM_UOM` varchar(20) NOT NULL,
  `POS_ITM_UOM` int(11) NOT NULL,
  `ITM_UOM_BS` varchar(20) NOT NULL,
  `POS_ITM_UOM_BS` int(11) NOT NULL,
  `SLS_PRICE` int(11) NOT NULL,
  `TAX_ID` int(11) NOT NULL,
  `TAX_VAL` int(11) NOT NULL,
  `DISC_TYPE` varchar(20) NOT NULL,
  `DISC_VAL` int(11) NOT NULL,
  `DISC_TOTAL_VAL` int(11) NOT NULL,
  `ITM_TOTAL_VAL` int(11) NOT NULL,
  `SYNC_FLG` varchar(20) NOT NULL,
  `USR_ID_CREATE` int(11) NOT NULL,
  `USR_ID_CREATE_DT` date NOT NULL,
  `USR_ID_MOD` int(11) NOT NULL,
  `USR_ID_MOD_DT` date NOT NULL,
  `DOC_DATE` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$sls$rma`
--

CREATE TABLE `intrm$sls$rma` (
  `CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(11) NOT NULL,
  `HO_ORG_ID` varchar(2) NOT NULL,
  `ORG_ID` varchar(2) NOT NULL,
  `POS_DOC_ID` int(11) NOT NULL,
  `DOC_ID` varchar(40) NOT NULL,
  `DOC_DT` date NOT NULL,
  `REF_INV_DOC_ID` varchar(40) NOT NULL,
  `EO_ID` int(11) NOT NULL,
  `POS_EO_ID` int(11) NOT NULL,
  `REF_VOUCHER_ID` varchar(40) NOT NULL,
  `SYNC_FLG` varchar(2) NOT NULL,
  `USR_ID_CREATE` int(11) NOT NULL,
  `USR_ID_CREATE_DT` date NOT NULL,
  `USR_ID_MOD` int(11) NOT NULL,
  `USR_ID_MOD_DT` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$sls$rma$itm`
--

CREATE TABLE `intrm$sls$rma$itm` (
  `CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(11) NOT NULL,
  `HO_ORG_ID` varchar(2) NOT NULL,
  `ORG_ID` varchar(2) NOT NULL,
  `POS_DOC_ID` int(11) NOT NULL,
  `DOC_ID` varchar(40) NOT NULL,
  `ITM_ID` varchar(40) NOT NULL,
  `POS_ITM_ID` int(11) NOT NULL,
  `SR_NO` varchar(20) NOT NULL,
  `LOT_NO` varchar(40) NOT NULL,
  `ITM_QTY` int(11) NOT NULL,
  `ITM_UOM` varchar(20) NOT NULL,
  `POS_ITM_UOM` int(11) NOT NULL,
  `ITM_UOM_BS` varchar(20) NOT NULL,
  `POS_ITM_UOM_BS` int(11) NOT NULL,
  `SLS_PRICE` int(11) NOT NULL,
  `TAX_REVERSAL_FLG` varchar(1) NOT NULL,
  `TAX_ID` int(11) NOT NULL,
  `TAX_VAL` int(11) NOT NULL,
  `DISC_TYPE` varchar(1) NOT NULL,
  `DISC_VAL` int(11) NOT NULL,
  `DISC_TOTAL_VAL` int(11) NOT NULL,
  `ITM_TOTAL_VAL` int(11) NOT NULL,
  `SYNC_FLG` varchar(1) NOT NULL,
  `USR_ID_CREATE` int(11) NOT NULL,
  `USR_ID_CREATE_DT` date NOT NULL,
  `USR_ID_MOD` int(11) NOT NULL,
  `USR_ID_MOD_DT` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$usr`
--

CREATE TABLE `intrm$usr` (
  `SLOC_ID` int(2) NOT NULL,
  `USR_ID` varbinary(2) NOT NULL,
  `POS_USR_ID` int(10) DEFAULT NULL,
  `USR_NAME` varchar(100) NOT NULL,
  `USR_FST_NAME` varchar(20) DEFAULT NULL,
  `USR_LST_NAME` varchar(20) DEFAULT NULL,
  `USR_PHN_NO` int(10) NOT NULL,
  `USR_GNDR` varchar(10) DEFAULT NULL,
  `USR_IMG` varchar(30) DEFAULT NULL,
  `USR_ALIAS` varchar(50) DEFAULT NULL,
  `USR_PWD` varchar(100) NOT NULL,
  `USR_ACTV` varchar(1) NOT NULL,
  `USR_CONTACT_NO` varchar(20) NOT NULL,
  `CREATE_FLG` varchar(1) DEFAULT NULL,
  `UPD_FLG` varchar(1) DEFAULT NULL,
  `DEL_FLG` varchar(1) DEFAULT NULL,
  `USR_ID_CREATE` int(10) NOT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(10) NOT NULL,
  `USR_ID_MOD_DT` date NOT NULL,
  `USR_DISC` int(11) DEFAULT NULL,
  `ORG_ID` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$usr$role`
--

CREATE TABLE `intrm$usr$role` (
  `USR_ROLE_CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(2) NOT NULL,
  `USR_ROLE_ID` int(5) NOT NULL,
  `POS_USR_ROLE_ID` int(5) NOT NULL,
  `USR_ROLE_NM` varchar(50) NOT NULL,
  `USR_ROLE_ACTV` varchar(1) NOT NULL,
  `CREATE_FLG` varchar(1) NOT NULL,
  `UPD_FLG` varchar(1) NOT NULL,
  `DEL_FLG` varchar(1) NOT NULL,
  `USR_ID_CREATE` int(10) NOT NULL,
  `USR_ID_CREATE_DT` date NOT NULL,
  `USR_ID_MOD` int(10) NOT NULL,
  `USR_ID_MOD_DT` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$usr$role$lnk`
--

CREATE TABLE `intrm$usr$role$lnk` (
  `USR_ROLE_CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(2) NOT NULL,
  `USR_ROLE_ID` int(5) NOT NULL,
  `USR_ORG_ID` varchar(2) NOT NULL,
  `USR_ID` int(10) NOT NULL,
  `CREATE_FLG` varchar(1) NOT NULL,
  `UPD_FLG` varchar(1) NOT NULL,
  `DEL_FLG` varchar(1) NOT NULL,
  `USR_ID_CREATE` int(10) NOT NULL,
  `USR_ID_CREATE_DT` date NOT NULL,
  `USR_ID_MOD` int(10) NOT NULL,
  `USR_ID_MOD_DT` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrmeocatg`
--

CREATE TABLE `intrmeocatg` (
  `CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(5) NOT NULL,
  `HO_ORG_ID` varchar(2) NOT NULL,
  `EO_TYPE` varchar(5) NOT NULL,
  `EO_CATG_ID` varchar(50) DEFAULT NULL,
  `POS_EO_CATG_ID` int(5) DEFAULT NULL,
  `CATG_NM` varchar(50) DEFAULT NULL,
  `CREATE_FLG` varchar(1) DEFAULT NULL,
  `UPD_FLG` varchar(1) DEFAULT NULL,
  `DEL_FLG` varchar(1) DEFAULT NULL,
  `USR_ID_CREATE` int(10) NOT NULL,
  `USR_ID_CREATE_DT` date NOT NULL,
  `USR_ID_MOD` int(10) NOT NULL,
  `USR_ID_MOD_DT` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sheet 1`
--

CREATE TABLE `sheet 1` (
  `CLD_ID` int(4) DEFAULT NULL,
  `SLOC_ID` int(1) DEFAULT NULL,
  `HO_ORG_ID` int(2) DEFAULT NULL,
  `POS_ITM_ID` varchar(10) DEFAULT NULL,
  `ITM_ID` varchar(12) DEFAULT NULL,
  `UNIVERSAL_ITM_ID` varchar(12) DEFAULT NULL,
  `ITM_DESC` varchar(42) DEFAULT NULL,
  `ITM_LONG_DESC` varchar(42) DEFAULT NULL,
  `ITM_LEGACY_CODE` varchar(12) DEFAULT NULL,
  `GRP_ID` int(3) DEFAULT NULL,
  `UOM_BASIC` varchar(3) DEFAULT NULL,
  `UOM_SLS` varchar(3) DEFAULT NULL,
  `PRICE_BASIC` varchar(6) DEFAULT NULL,
  `PRICE_SLS` varchar(6) DEFAULT NULL,
  `SERIALIZED_FLG` varchar(1) DEFAULT NULL,
  `USR_ID_CREATE` int(1) DEFAULT NULL,
  `USR_ID_CREATE_DT` varchar(10) DEFAULT NULL,
  `USR_ID_MOD` int(1) DEFAULT NULL,
  `USR_ID_MOD_DT` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
