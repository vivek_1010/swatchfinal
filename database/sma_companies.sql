-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 28, 2016 at 08:20 AM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `swatch_final`
--

-- --------------------------------------------------------

--
-- Table structure for table `sma_companies`
--

CREATE TABLE `sma_companies` (
  `id` int(11) NOT NULL,
  `group_id` int(10) UNSIGNED DEFAULT NULL,
  `group_name` varchar(20) NOT NULL,
  `customer_group_id` int(11) DEFAULT NULL,
  `customer_group_name` varchar(100) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `company` varchar(255) DEFAULT NULL,
  `vat_no` varchar(100) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(55) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `state` varchar(55) DEFAULT NULL,
  `postal_code` varchar(8) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `salutation` varchar(10) DEFAULT NULL,
  `cf1` varchar(100) DEFAULT NULL,
  `cf2` varchar(100) DEFAULT NULL,
  `cf3` varchar(100) DEFAULT NULL,
  `cf4` varchar(100) DEFAULT NULL,
  `cf5` varchar(100) DEFAULT NULL,
  `cf6` varchar(100) DEFAULT NULL,
  `invoice_footer` text,
  `payment_term` int(11) DEFAULT '0',
  `logo` varchar(255) DEFAULT 'logo.png',
  `award_points` int(11) DEFAULT '0',
  `lname` varchar(50) NOT NULL,
  `type` int(5) NOT NULL DEFAULT '0',
  `upd_flg` tinyint(1) NOT NULL,
  `eo_type` varchar(1) DEFAULT NULL,
  `eo_id` varchar(20) DEFAULT NULL,
  `sync_flg` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_companies`
--

INSERT INTO `sma_companies` (`id`, `group_id`, `group_name`, `customer_group_id`, `customer_group_name`, `name`, `company`, `vat_no`, `address`, `city`, `state_id`, `country_id`, `state`, `postal_code`, `country`, `phone`, `email`, `gender`, `salutation`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `invoice_footer`, `payment_term`, `logo`, `award_points`, `lname`, `type`, `upd_flg`, `eo_type`, `eo_id`, `sync_flg`) VALUES
(1, 3, 'customer', 1, 'General', 'Walk-in Customer', 'Walk-in Customer', '', 'Customer Address', 'Petaling Jaya', NULL, NULL, 'Selangor', '46000', 'Malaysia', '0123456789', 'customer@tecdiary.com', '', '', '', '', '', '', '', '', NULL, 0, 'logo.png', 0, '', 0, 0, NULL, NULL, NULL),
(123, NULL, 'biller', NULL, NULL, 'SGIRPL Swatch Store Phoenix-Pune', 'SGIRPL Swatch Store Phoenix-Pune', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'INDIA', '0123456789', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, NULL, NULL, NULL),
(124, NULL, 'biller', NULL, NULL, 'SGIRPL Swatch Store Express AV-Chennai', 'SGIRPL Swatch Store Express AV-Chennai', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'INDIA', '0123456789', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, NULL, NULL, NULL),
(125, NULL, 'biller', NULL, NULL, 'SGIRPL Swatch Store Phoenix-Mumbai', 'SGIRPL Swatch Store Phoenix-Mumbai', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'INDIA', '0123456789', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, NULL, NULL, NULL),
(126, 3, 'customer', 1, 'General', 'Th kim', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '9765499272', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '17', '1'),
(127, 3, 'customer', 1, 'General', 'Nicku', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '9890022114', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '27', '1'),
(128, 3, 'customer', 1, 'General', 'jyotsna', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '8975151386', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '4', '1'),
(129, 3, 'customer', 1, 'General', 'Sheth Rujuta', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '7507066134', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '26', '1'),
(130, 3, 'customer', 1, 'General', 'Disha Shah', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '9765170832', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '32', '1'),
(131, 3, 'customer', 1, 'General', 'Gorat', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '9890888421', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '19', '1'),
(132, 3, 'customer', 1, 'General', 'T. J. Srinivasaraj', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '9840064793', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '24', '1'),
(133, 3, 'customer', 1, 'General', 'Shabir Saiyad', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '9483092091', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '18', '1'),
(134, 3, 'customer', 1, 'General', 'Sabu', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '8806415577', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '23', '1'),
(135, 3, 'customer', 1, 'General', 'Kartik Arun', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '9823420002', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '25', '1'),
(136, 3, 'customer', 1, 'General', 'Danuiela Raimondi', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '9168302200', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '8', '1'),
(137, 3, 'customer', 1, 'General', 'Reinhard stierl', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '7507066134', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '13', '1'),
(138, 3, 'customer', 1, 'General', 'Sahil', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '8806793233', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '20', '1'),
(139, 3, 'customer', 1, 'General', 'Vidya', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '9172710420', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '21', '1'),
(140, 3, 'customer', 1, 'General', 'Poorvi Sedani', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '9923003800', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '22', '1'),
(141, 3, 'customer', 1, 'General', 'Ajay Chauhan', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '7798808399', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '29', '1'),
(142, 3, 'customer', 1, 'General', 'Goldy', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '9146333425', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '35', '1'),
(143, 3, 'customer', 1, 'General', 'rajesh', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '9538332233', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '7', '1'),
(144, 3, 'customer', 1, 'General', 'ANKITA', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '8149867942', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '30', '1'),
(145, 3, 'customer', 1, 'General', 'Regina', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '9764112043', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '31', '1'),
(146, 3, 'customer', 1, 'General', 'Rajiv Mathur', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '9892109522', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '1', '1'),
(147, 3, 'customer', 1, 'General', 'kapilkapoor', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '9822029610', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '2', '1'),
(148, 3, 'customer', 1, 'General', 'AAMAN', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '9980566299', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '3', '1'),
(149, 3, 'customer', 1, 'General', 'Sumit Papani', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '9850491202', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '9', '1'),
(150, 3, 'customer', 1, 'General', 'Gaurav Jain', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '9928082984', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '10', '1'),
(151, 3, 'customer', 1, 'General', 'chetan sonawane', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '9403727626', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '5', '1'),
(152, 3, 'customer', 1, 'General', 'Btlinia', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '9748340093', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '6', '1'),
(153, 3, 'customer', 1, 'General', 'Shantunu', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '8826721674', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '11', '1'),
(154, 3, 'customer', 1, 'General', 'Junaid', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '9711637417', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '12', '1'),
(155, 3, 'customer', 1, 'General', 'Agrwal', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '9765070101', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '28', '1'),
(156, 3, 'customer', 1, 'General', 'Shantunu Ghosh', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '9823016246', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '33', '1'),
(157, 3, 'customer', 1, 'General', 'Akash', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '9300022226', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '14', '1'),
(158, 3, 'customer', 1, 'General', 'Pantiki', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '9820251000', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '15', '1'),
(159, 3, 'customer', 1, 'General', 'Tanvi', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '8904877202', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '16', '1'),
(160, 3, 'customer', 1, 'General', 'Preeti', 'Swatch Group', NULL, '       ', NULL, NULL, NULL, NULL, NULL, NULL, '9860811111', 'Not Available', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0, 'C', '34', '1'),
(189, 3, 'customer', 1, 'General', 'ajay', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9793276740', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0, NULL, NULL, NULL),
(190, 3, 'customer', 1, 'General', 'ankit ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9005678909', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0, NULL, NULL, NULL),
(191, 3, 'customer', 1, 'General', 'ajay', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8527536265', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0, NULL, NULL, NULL),
(192, 3, 'customer', 1, 'General', 'ajay', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8527536265', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0, NULL, NULL, NULL),
(193, 3, 'customer', 1, 'General', 'manku', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9876543210', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0, NULL, NULL, NULL),
(194, 3, 'customer', 1, 'General', 'manku', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9876543210', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0, NULL, NULL, NULL),
(195, 3, 'customer', 1, 'General', 'ramu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9876543201', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0, NULL, NULL, NULL),
(196, 3, 'customer', 1, 'General', 'rohan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9123456789', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0, NULL, NULL, NULL),
(197, 3, 'customer', 1, 'General', 'manku', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9876543210', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0, NULL, NULL, NULL),
(198, 3, 'customer', 1, 'General', 'Rishan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9999999999', 'abcj@gmail.com', 'M', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, 'Khan', 0, 0, NULL, NULL, NULL),
(199, 3, 'customer', 4, 'New Customer (+10)', 'Salimii', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9090909090', 'nknqknskqn@gma.com', 'F', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, 'J', 0, 0, NULL, NULL, NULL),
(200, 3, 'customer', 1, 'General', 'Ajay', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8527536265', 'ajay.kesharwani@essindia.com', 'M', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, 'Kesharwani', 0, 0, NULL, NULL, NULL),
(201, 3, 'customer', 1, 'General', 'ajay', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9005432654', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0, NULL, NULL, NULL),
(202, 3, 'customer', 1, 'General', 'John', NULL, NULL, NULL, NULL, 1475, 99, NULL, NULL, NULL, '9898989898', 'j@essindia.com', 'M', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, 'Stewart', 0, 1, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sma_companies`
--
ALTER TABLE `sma_companies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `group_id_2` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sma_companies`
--
ALTER TABLE `sma_companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=203;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
