<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?= $page_title . " " . lang("no") . " " . $inv->id; ?></title>
        <base href="<?= base_url() ?>"/>
        <meta http-equiv="cache-control" content="max-age=0"/>
        <meta http-equiv="cache-control" content="no-cache"/>
        <meta http-equiv="expires" content="0"/>
        <meta http-equiv="pragma" content="no-cache"/>
        <link rel="shortcut icon" href="<?= $assets ?>images/icon.png"/>
        <link rel="stylesheet" href="<?= $assets ?>styles/theme.css" type="text/css"/>
        
        <style type="text/css" media="all">
            body {
                color: #000;
            }
            
            .rupee{
                font-family: Arial;
                margin-right: 5px;
            }
            
            .right-space{margin:0 10px;}
            
            hr {
                -moz-border-bottom-colors: none;
                -moz-border-image: none;
                -moz-border-left-colors: none;
                -moz-border-right-colors: none;
                -moz-border-top-colors: none;
                border-color: #EEEEEE -moz-use-text-color #FFFFFF;
                border-style: solid none;
                border-width: 1px 0;
                margin: 5px 0;
            }        
            .stable{
                font-size:11px;
                padding:0px !important;
            }
            #wrapper {
                max-width: 680px;
                margin: 0 auto;
                padding-top: 20px;
            }
            
            .font-weight-bold{
                font-weight: bold;
            }

            .btn {
                border-radius: 0;
                margin-bottom: 5px;
            }

            h3 {
                margin: 5px 0;
            }
			
			img.center {
				display: block;
				margin: 0 auto;
			}

            @media print {
                .no-print {
                    display: none;
                }

                #wrapper {
                    max-width: 680px;
                    width: 100%;
                    min-width: 250px;
                    margin: 0 auto;
                }
            }
			.declaration{
				font-size:11px;
			}
        </style>
    </head>
    
    <body>
    <?php //} ?>
<div id="wrapper">
    <div id="receiptData">
        <div class="text-center">
            <img src="<?= base_url() . 'assets/uploads/logos/' . $biller->logo; ?>" 
                    alt="<?= $biller->company; ?>" style="height:20px;">
        </div>
        <div class="text-right">   
        <?php
            if ($pos_settings->cf_title1 != "" && $pos_settings->cf_value1 != "") {
               echo $pos_settings->cf_title1 . ": " . $pos_settings->cf_value1."<br/>";
            }
            if ($pos_settings->cf_title2 != "" && $pos_settings->cf_value2 != "") {
                echo $pos_settings->cf_title2 . ": " . $pos_settings->cf_value2."<br/>";
            }
            
             //echo 'TIN: '.$biller->tin_no;
         ?>
        </div>
        <div class="row" style="width:auto !important;margin:0px;">
            <div class="col-sm-12 text-center">
                <h5 style="font-weight:bold;"><?= lang('tax_invoice'); ?></h5>
            </div>
        </div>
        
        <div class="row" style="width:auto !important;margin:0px;">
            <div class="col-md-12 text-left" style="font-size:11px;padding:0px !important;">
                <table class="stable">
                    <tr><td><strong><?= lang("store_name"); ?></strong> : <?php echo $biller->company; ?></td></tr>
                    <tr><td><strong><?= lang("store_address"); ?></strong> : <?php echo $biller->address.'&nbsp;&nbsp;(<b>TIN:</b> '.$biller->tin_no.')'; ?></td></tr>
                </table>
                <hr>
                <table class="stable">
                     <?php
                /* Updates done by Chitra to modify the receipt format */
                echo "<tr><td><strong>" . lang("date") . "</strong> : " . $this->sma->hrld($inv->date)."</td></tr>";
                echo "<tr><td><strong>" . lang("invoice_number") . "</strong> : " . $inv->reference_no."</td></tr>";
                //echo "<tr><td><strong>" . lang("sales_exe_id") . "</strong> : " . ucwords($inv->first_name)." ". ucwords($inv->last_name) ." (".$inv->sales_executive_id.")"."</td></tr>";
                echo "<tr><td><strong>" . lang("sales_exe_id") . "</strong> : " .$inv->username."</td></tr>";
                ?>
                </table>
               
                <hr>
                <table class="stable">
                    <tr><td><strong><?= lang("customer_name"); ?></strong> : <?php echo $inv->customer; ?></td></tr>
                    <tr><td><strong><?= lang("customer_contact"); ?></strong> : <?php echo ($inv->sale_status === 'foc') ? $foc_details->mobile:$customer->phone; ?></td></tr>
                </table>
                <hr></hr>
            </div>
        </div>
        
        <table class="table table-responsive table-bordered">
            <thead>
                 <tr>
                    <th class='col-xs-3 text-left'>Item Code</th>
                    <th class='col-xs-3 text-left'>HSN Code</th>
                    <th class='col-xs-3 text-left'>Dis <span class="rupee">&#8377;</span></th>
                    <th class='col-xs-3 text-left'><?= lang("tax"); ?></th>
                    <th class='col-xs-3 text-right'><?= lang("mrp"); ?> <span class="rupee">&#8377;</span></th>
                </tr>
            </thead>
            
            <tbody>
            <?php
                $r = 1;
                $tax_summary = array();
                $total_discounted_amount = 0;
                $total_net_amt = 0;
                $total_amt = 0;
                $total_items = 0; 
                $total_discount = 0;
                $total_tax = 0;
                $total_order_tax = 0;
                $prod_total_amount = 0;
                $total_mrp = 0;
                foreach ($rows as $row) {
                    $total_discounted_amount += ($row->quantity * $row->net_unit_price);
                    if($row->pdiscount_type === 'foc'){
                        $total_net_amt += ($row->unit_price*0) - $row->item_discount;
                    }else{
                        $total_net_amt += ($row->quantity * $row->net_unit_price) - $row->item_discount;
                    }
                    
                    $total_amt += $row->net_unit_price;
                    $total_items +=$row->quantity;
                    $total_discount+= $row->item_discount;
                    $total_tax += $row->item_tax;
                    $total_mrp += $row->real_unit_price;
                    
                    $tax_summary[$row->tax_code]['items'] = $row->quantity;
                    $tax_summary[$row->tax_code]['tax'] = $row->item_tax;
                    $tax_summary[$row->tax_code]['amt'] = ($row->quantity * $row->net_unit_price) - $row->item_discount;
                    $tax_summary[$row->tax_code]['name'] = $row->tax_name;
               
                        $tax_summary[$row->tax_code]['code'] = $row->tax_code;
                        $tax_summary[$row->tax_code]['rate'] = $row->tax_rate;
                   
                    /*Updated By Chitra to calculate total tax of the inline items added. */
                    //echo "<pre>";print_r($tax_summary);
                    if($pos->order_discount_type == 'percent'){
                        if($row->pdiscount_type === 'foc'){
                            $disc = $row->unit_price;
                        }else{
                            $disc = ($row->real_unit_price * $row->discount)/100;
                        }
                    }
                    else{
                        $disc = $row->item_discount;
                    }
                    
                    if($row->pdiscount_type === 'foc'){
                            $prod_amount = $this->sma->formatMoney($row->unit_price);
                    }else{
                            $prod_amount = $this->sma->formatMoney($row->net_unit_cost+$row->item_tax);
                    }
                    
                    if($row->pdiscount_type === 'foc'){
                        $prod_total_amount+= $row->unit_price;
                    }else{
                        $prod_total_amount+= $row->net_unit_cost+$row->item_tax;
                    }
                    $prod_tax_rate = $this->sma->formatMoney($tax_summary[$row->tax_code]['rate']);
                    $prod_price = $tax_summary[$row->tax_code]['amt']; 
                    $total_order_tax +=($tax_summary[$row->tax_code]['tax']);
                    $prod_tax = ($prod_price*$prod_tax_rate)/((100)+($prod_tax_rate));
                    $booking_status = !empty($row->advance_booking) ? '(Advance Booking)' : ''; 
                    $row->product_hsn = $row->product_hsn === null ? '' : $row->product_hsn;
                    if($row->return_id != 0){
                        echo '<tr>'
                        . '<td class="text-left">' .($row->product_code). ($row->variant ? ' (' . $row->variant . ')' : '').'</td>'
                        . '<td class="text-left">'.$this->sma->formatMoney($row->product_hsn).'</td>'
                        . '<td class="text-left">' .$this->sma->formatMoney($disc). '</td>'
                        . '<td class="text-left"><span class="rupee">&#8377;</span>'.$this->sma->formatMoney($tax_summary[$row->tax_code]['tax']).''
                        . '('.$this->sma->formatMoney($tax_summary[$row->tax_code]['rate']).'%)</td>'
//                        . '<td class="text-left">' . $prod_amount . '</td>'
                          .'<td class="text-right">'.$this->sma->formatMoney($row->real_unit_price).'</td>'
                        . '</tr>';
                    }
                    else{
                        echo '<tr>'
                        . '<td class="text-left">' . ($row->product_code). ($row->variant ? ' (' . $row->variant . ')' : '').'</td>'
                        . '<td class="text-left">'.$this->sma->formatMoney($row->product_hsn).'</td>'
                        . '<td class="text-left">' .$this->sma->formatMoney($disc). '</td>'
                        . '<td class="text-left"><span class="rupee">&#8377;</span>'.$this->sma->formatMoney($tax_summary[$row->tax_code]['tax']).''
                        . '('.$this->sma->formatMoney($tax_summary[$row->tax_code]['rate']).'%)</td>'
//                        . '<td class="text-left">' . $prod_amount . '</td>'
                        .'<td class="text-right">'.$this->sma->formatMoney($row->real_unit_price).'</td>'
                        . '</tr>';
                    }
                    //echo '</td><td>'.$this->sma->formatMoney($row->item_discount).'</td><td>'.$this->sma->formatMoney($row->tax).'</td><td>'.$this->sma->formatMoney($row->item_tax).'</td><td>'.intval($row->quantity).'</td><td class="text-left">' . $this->sma->formatMoney($row->unit_price + $row->item_discount) . '</td></tr>';
                    
                    $r++;
                }
                
                ?>
            </tbody>
            
            <tfoot>
                <tr>
                    <th colspan="3" class="text-left"><?= lang("total"); ?></th>
                    <th class="text-center"><?=intval($total_items)?></th>
                    <th class='text-right'><?=$this->sma->formatMoney($total_mrp)?></th>
                    <!--<th class="text-right"><?=$this->sma->formatMoney($inv->total + $inv->product_tax)?></th>-->
                </tr>
                    <tr>
                        <th colspan="4" class="text-left"><?= lang("discount")?></th>
                        <th class='text-right'><?=$this->sma->formatMoney($total_discount)?></th>
                    </tr>
                    <tr>
                        <th colspan="4" class="text-left"><?= lang("basic")?></th>
                        <th class='text-right'><?=$this->sma->formatMoney($total_net_amt+$total_discount)?></th>
                    </tr>
                    <tr>
                        <th colspan="4" class="text-left"><?= lang("tax_included")?></th>
                        <th class='text-right'><?=$this->sma->formatMoney($inv->total_tax)?></th>
                    </tr>
                <?php
                //echo "<pre>";print_r($inv);
                if ($inv->order_tax != 0) {
                    echo '<tr><th colspan="4" class="text-left">' . lang("order_tax") . '</th>'
                            . '<th class="text-right">' . $this->sma->formatMoney($inv->order_tax) . '</th></tr>';
                }
                if ($inv->order_discount != 0) {
                    echo '<tr><th colspan="4" class="text-left">' . lang("order_discount") . '</th>'
                            . '<th class="text-right">'.$this->sma->formatMoney($inv->order_discount) . '</th></tr>';
                }
                
                if ($pos_settings->rounding) {
                    
                    $round_total = $this->sma->roundNumber($inv->grand_total, $pos_settings->rounding);
                    $rounding = $this->sma->formatMoney($round_total - $inv->grand_total);
                ?>
                    <tr>
                        <th colspan="4"><?= lang("rounding"); ?></th>
                        <th class="text-right"><?= $rounding; ?></th>
                    </tr>
                    <tr>
                        <th colspan="4" class="text-left"><?= lang("net_amount"); ?></th>
                        <th class="text-right"><?= $this->sma->formatMoney($inv->grand_total + $rounding); ?></th>
                    </tr>
                     <?php if($inv->grand_total > 0){?>
                    <tr>
                        <th colspan="2" class="text-left"><?= lang("amount_chargeable_words"); ?></th>
                        <th colspan="3" class="text-left"><?=$this->pos_model->convert_numbers_to_words($inv->grand_total + $rounding)?></th>
                    </tr>
                     <?php }?>
                <?php } else { 
                        if($inv->grand_total > 0){
                    ?>
                    
                    <tr>
                        <th colspan="4" class="text-left"><?= lang("net_amount_pdf"); ?></th>		
                        <th colspan="2" class="text-right"><?= $this->sma->formatMoney(round($inv->grand_total,0)); ?></th>

                    </tr>
                        <?php }?>
            <tr>
            </tfoot>
        </table>
        <hr>
            <?php 
            //echo "<pre>";print_r($payments);
            if ($payments) {
               echo '<table class="table table-condensed"><tbody>';
                foreach ($payments as $payment) {
                    if($payment->pos_paid > 0){
                    echo '<tr>';
                    if ($payment->paid_by == 'cash' && $payment->pos_paid) {
                        echo '<td style="display:none;"></td>';
                        echo '<td colspan="2">' . lang("paid_by") . ': ' . lang($payment->paid_by) . '</td>';
                        echo '<td colspan="2">' . lang("amount") . ':  <span class="rupee">&#8377;</span> ' . $this->sma->formatMoney($payment->pos_paid == 0 ? $payment->amount : $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                        //echo '<td>' . lang("change") . ': ' . ($payment->pos_balance > 0 ? $this->sma->formatMoney($payment->pos_balance) : 0) . '</td>';
                        
                    }
                    if (($payment->paid_by == 'CC' || $payment->paid_by == 'ppp' || $payment->paid_by == 'stripe') && $payment->cc_no) {
                        echo '<td style="display:none;"></td>';
                        echo '<td colspan="1">';
                        echo '<span class="right-space">' . lang("paid_by") . ': ' . lang($payment->card_type) . '</span></td>';
                        echo '<td colspan="2"><span class="right-space rupee">' . lang("amount") . ': &#8377;' . $this->sma->formatMoney($payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</span></td>';
                        echo '<td colspan="2"><span class="right-space">' . lang("no") . ': ' .substr($payment->cc_no, 0, 4). '-xx-' . substr($payment->cc_no, -4) .'</span></td>';
                        echo '<td><span class="right-space">' . lang("expiry") . ': ' .$payment->cc_month.'/'.$payment->cc_year.'</span></td>';
                       //echo '<td>' . lang("name") . ': ' . $payment->cc_holder . '</td>';
                    }
                    if ($payment->paid_by == 'Cheque' && $payment->cheque_no) {
                        echo '<td style="display:none;"></td>';
                        echo '<td colspan="2">' . lang("paid_by") . ': ' . lang($payment->paid_by) . '</td>';
                        echo '<td>' . lang("amount") . ':  <span class="rupee">&#8377;</span> ' . $this->sma->formatMoney($payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                        echo '<td>' . lang("cheque_no") . ': ' . $payment->cheque_no . '</td>';
                    }
                    if ($payment->paid_by == 'credit_voucher' && $payment->pos_paid) {
                        echo '<td class="text-left">' . lang("paid_by") . ': ' . $payment->paid_by . '</td>';
                        echo '<td colspan="3" class="text-right" style="padding-left:10px;">  ' . lang("no") . ': ' . $payment->cc_no . '</td>';
                        echo '<td class="text-right">' . lang("amount") . ': <span class="rupee">&#8377;</span> ' . $this->sma->formatMoney($payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                        echo '<td class="text-center">' . lang("credit_note_balance") . ': ' . ($payment->cn_balance > 0 ? $this->sma->formatMoney($payment->cn_balance) : 0) . '</td>';     
                    }
                    if ($payment->paid_by == 'other' && $payment->amount) {
                        echo '<td style="display:none;"></td>';
                        echo '<td colspan="2">' . lang("paid_by") . ': ' . lang($payment->paid_by) . '</td>';
                        echo '<td>' . lang("amount") . ':  <span class="rupee">&#8377;</span> ' . $this->sma->formatMoney($payment->pos_paid == 0 ? $payment->amount : $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                        echo $payment->note ? '</tr><td colspan="2">' . lang("payment_note") . ': ' . $payment->note . '</td>' : '';
                    }
                    echo '</tr>';
                    }
                }
                echo '</tbody></table>';
            }

            /*if ($Settings->invoice_view == 1) {
                if (!empty($tax_summary)) {
                    echo '<h4 style="font-weight:bold;">' . lang('tax_summary') . '</h4>';
                    echo '<table class="table table-condensed"><thead><tr><th>' . lang('name') . '</th><th>' . lang('code') . '</th><th>' . lang('qty') . '</th><th>' . lang('tax_excl') . '</th><th>' . lang('tax_amt') . '</th></tr></td><tbody>';
                    foreach ($tax_summary as $summary) {
                        echo '<tr><td>' . $summary['name'] . '</td><td class="text-center">' . $summary['code'] . '</td><td class="text-center">' . $this->sma->formatQuantity($summary['items']) . '</td><td class="text-right">' . $this->sma->formatMoney($summary['amt']) . '</td><td class="text-right">' . $this->sma->formatMoney($summary['tax']) . '</td></tr>';
                    }
                    echo '</tbody></tfoot>';
                    echo '<tr><th colspan="4" class="text-right">' . lang('total_tax_amount') . '</th><th class="text-right">' . $this->sma->formatMoney($inv->product_tax) . '</th></tr>';
                    echo '</tfoot></table>';
                }
            }*/
            ?>  
        <hr>
                <?php }
                if ($inv->paid < $inv->grand_total) { ?>
                <table>
                    <tr>
                        <th colspan="7"><?= lang("paid_amount"); ?></th>					
                        <th class="text-left" ><span class="rupee">&#8377;</span><?= $this->sma->formatMoney($inv->paid); ?></th>
                    </tr>
                    <tr>
                        <th colspan="7"><?= lang("due_amount"); ?></th>
                        <th class="text-left"><span class="rupee">&#8377;</span><?= $this->sma->formatMoney($inv->grand_total - $inv->paid); ?></th>
                    </tr>
                </table>       
                    
                <?php } ?>

        <table cellpadding="5">
            <?php if($inv->grand_total > 0){?>
             <tr>
                <td colspan="9" class="text-left font-weight-bold"><?= lang("amount_chargeable_words"); ?></td>
             </tr>
             <tr>
<!--            <th class="text-left" colspan="6"><?=$this->sma->formatMoney($inv->grand_total + $rounding)?></th>-->
                <td class="text-left font-weight-bold" colspan="9"><?=ucwords($this->pos_model->convert_numbers_to_words($inv->grand_total));?></td>
            </tr>
            <?php }?>
            <table class="table">
                <thead>
                    <tr>
                        <th style="font-weight:normal !important; font-size:9px;">
                              <h5 class="text-center">Declaration </h5>
                                                    <?php if(($biller->cf1 != NULL) || ($biller->cf1 != '')){?>
                            <p class="declaration text-justify">1. <?=$biller->cf1?></p>
                                                    <?php }?>
                                                    <?php if(($biller->cf2 != NULL) || ($biller->cf2 != '')){?>
                            <p class="declaration text-justify">2. <?=$biller->cf2?></p>
                                                    <?php }?>
                                                    <?php if(($biller->cf2 == NULL) || ($biller->cf2 == '')){?>
                            <p class="declaration text-justify"><strong>2. No returns/refunds, watches can be exchanged within 
                            <?=$inv->return_time;?> days from the date of purchase.</strong></p>
                                                    <?php }else{?>
                                                      <p class="declaration text-justify"><strong>3. No returns/refunds, watches can be exchanged within 
                            <?=$inv->return_time;?> days from the date of purchase.</strong></p>
                                                    <?php }?>
                        </th>
                    </tr>
                    <tr>
                       <th class="text-center">* This is a computer generated invoice, hence does not require any signature</th>
                   </tr>
                </thead>
            </table>               
        </table>
       
          <?= $inv->note ? '<p class="text-center">' . $this->sma->decode_html($inv->note) . '</p>' : ''; ?>
        <?= $inv->staff_note ? '<p class="no-print"><strong>' . lang('staff_note') . ':</strong> ' . $this->sma->decode_html($inv->staff_note) . '</p>' : ''; ?>
        <div class="well well-sm" style="font-size:9px !important;">
            <h6 class="text-center"><strong><?php echo $pos_settings->cf_title3; ?></strong></h6>
            <p class="text-center declaration"><strong>Registered Office : </strong><?php echo $pos_settings->cf_value3; ?></p>
            <p class="text-center declaration"><strong><?php echo $pos_settings->cf_title4; ?> : </strong> <?php echo $pos_settings->cf_value4; ?></p>
            <p class="text-center declaration"><strong><?php echo $pos_settings->cf_title5; ?> : </strong><?php echo $pos_settings->cf_value5; ?></p>
            <p class="text-center declaration"><strong><?php echo $pos_settings->cf_title6; ?> : </strong><?php echo $pos_settings->cf_value6; ?></p>
        </div>
<!--        <hr>-->
